PARALLEL ?= $(shell nproc)

OUT ?= out

# try to use schedtool & ionice (i.e. if installed)
TRY_SCHED ?= 1


# shouldn't have to change anything below this line
KERNELCMDLINE := ignore_loglevel
KERNELCMDLINE += earlycon=exynos4210,mmio32,0x10A00000 console=ttySAC0,3000000n8
KERNELCMDLINE += clk_ignore_unused
KERNELCMDLINE += printk.devkmsg=on
KERNELCMDLINE += log_buf_len=1024K
KERNELCMDLINE += at24.write_timeout=100

# this provides / uses a minimal prebuilt cpio image, but an alternative
# may be passed in via the PREBUILT_CPIO variable
PREBUILT_CPIO ?= $(PREBUILT_CPIO_BUSYBOX)

.PHONY: all
all: help

.PHONY: help
help:
	@echo "Build and optionally flash and/or boot Pixel 6 images"
	@echo "The following make targets exist:"
	@echo "    help              this help"
	@echo "    clean             clean all generated files"
	@echo ""
	@echo "    prepare-device    prepare device for development (Verity/UART/boot"
	@echo "                      partitions/etc.)"
	@echo "    run               directly boot (run) images (potentially rebuilding images"
	@echo "                      first)"
	@echo "                      Note: this will bypass any potentially flashed images"
	@echo "    rerun             directly boot (run) images (without rebuilding)"
	@echo "                      Note: this will bypass any potentially flashed images"
	@echo "    flash             flash and boot images (potentially rebuilding them first)"
	@echo "    reflash           flash and boot images (without rebuilding)"
	@echo "    flash-no-boot     flash but don't boot images (potentially rebuilding them"
	@echo "                      first)"
	@echo "    reflash-no-boot   flash but don't boot images (without rebuilding)"
	@echo ""
	@echo "    images            generate all flashable images (including kernel artefacts)"
	@echo "    build             build all kernel artefacts (only)"
	@echo ""
	@echo "    menuconfig        Linux menuconfig"
	@echo "    nconfig           Linux nconfig"
	@echo "    savedefconfig     Linux savedefconfig"
	@echo "    copydefconfig     savedefconfig and copy back into Linux source tree"
	@echo ""
	@echo "    dt_binding_check_vendors"
	@echo "    dt_binding_check_vendor_exynos"
	@echo "    dt_binding_check_vendor_google"
	@echo "    dt_binding_check_vendor_samsung"
	@echo "                      check only those DT bindings for vendors that we care"
	@echo "                      about for Pixel6."
	@echo "    dt_binding_check_all"
	@echo "                      check all DT bindings (this will likely complain about"
	@echo "                      unrelated bindings)"
	@echo "    dtbs_check_exynos validate all Samsung Exynos DTs (32 & 64 bit ARM)"
	@echo "    dtbs_check_oriole validate Pixel6 DTs"
	@echo "    validate_dt       run checks of DT bindings and DTs related to Pixel6"
	@echo ""
	@echo ""
	@echo "Notes regarding image / boot / flash targets:"
	@echo "  * To boot an image, dtbo.img, boot.img and vendor_boot.img should be in sync."
	@echo "      * dtbo.img doesn't usually change and we only need to flash it once"
	@echo "        (using the flash or reflash targets)"
	@echo "      * the 'run' and 'rerun' targets upload new images and run those instead of"
	@echo "        flashing, they will not run any images that potentially have been"
	@echo "        flashed to the device previously"
	@echo "      * if boot and vendor_boot partitions have been erased (as is the case"
	@echo "        after the 'prepare-device' target), the device will always boot up into"
	@echo "        fastboot, waiting for a new image, thereby ensuring that it never boots"
	@echo "        an old (out-of-date) image"
	@echo "      * for the 'run' or 'rerun' targets to work, you should have run the"
	@echo "        'prepare-device' target at least once first. (It is implicitly executed"
	@echo "        as part of the 'flash' targets and therefore does not need to be"
	@echo "        explicitly executed for those)"
	@echo "  * Taking the above into account, the 'run' or 'rerun' targets are usually the"
	@echo "    correct targets to use"
	@echo ""
	@echo "Notes regarding DT bindings:"
	@echo "  * Specifying multiple targets in one make invocation is not supported,"
	@echo "    at least with -j (without -j, things become very slow, though). It's"
	@echo "    recommended to either"
	@echo "      a) run the top-level targets dt_binding_check_vendors or validate_dt, or"
	@echo "      b) run the individual ones sequentially."
	@echo ""
	@echo "Notes regarding cpio images:"
	@echo "  * By default, this is building a minimal cpio initramfs image based on some"
	@echo "    prebuilt binaries. This might not be suitable in all cases, so alternatively"
	@echo "    the PREBUILT_CPIO make-variable can be used to point to a different cpio."
	@echo "    That cpio should be a fully useable initramfs file system as desired, A copy"
	@echo "    of that cpio will be modified to include kernel+modules and will then be"
	@echo "    used instead."
	@echo ""
	@echo "There are also a few internal targets, but you should not need to"
	@echo "invoke them manually, hence they're not described here."
	@echo ""
	@echo "Note: While building, we use schedtool and ionice (if available) to"
	@echo "      increase interactive responsiveness of the system."


.PHONY: clean
clean:
	rm -rf $(OUT)


THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

ifeq ($(TRY_SCHED),1)
SCHED := $(shell command -pv schedtool)
ifneq ($(SCHED),)
SCHED += -B -n20 -e
endif # SCHED

IONICE := $(shell command -pv ionice)
ifneq ($(IONICE),)
IONICE += -c3
endif # IONICE
endif # TRY_SCHED

ifeq ($(KGZIP),)
KGZIP := pigz
endif # KGZIP

ifeq ($(origin LD),default)
LD := $(shell command -pv ld.lld)
ifeq ($(LD),)
LD := aarch64-linux-gnu-ld.bfd
endif # LD
endif # LD
STRIP := aarch64-linux-gnu-strip


MKDTIMG := tools/prebuilts/linux-x86/bin/mkdtimg
MKBOOTIMG := tools/mkbootimg/mkbootimg.py
BOARDDTB := gs101-oriole.dtb

LINUX ?= src/linux
KERNEL_OUT ?= $(shell realpath --relative-to=$(LINUX) $(OUT))
KERNEL_MAKE = $(MAKE) -C $(LINUX) KGZIP=$(KGZIP) ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- LD='$(LD)' O=$(KERNEL_OUT)/linux

$(OUT)/FORCE:

KERNEL_STAMP := $(OUT)/.kernel.stamp
$(KERNEL_STAMP): $(OUT)/FORCE | $(OUT)
	export GIT_INDEX_FILE=$(KERNEL_OUT)/$(@F).gitindex \
	&& cd "$(LINUX)" \
	&& git_dir="$$(git rev-parse --git-dir)" \
	&& cp "$${git_dir}/index" "$${GIT_INDEX_FILE}" \
	&& git --git-dir="$${git_dir}" add -A . \
	&& git --git-dir="$${git_dir}" write-tree > $(KERNEL_OUT)/$(@F).tmp \
	&& rm "$${GIT_INDEX_FILE}"
	echo "$(KERNEL_MAKE)" >> $@.tmp
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

KERNEL_DOTCONFIG := $(OUT)/linux/.config
$(KERNEL_DOTCONFIG): src/pixelscripts/gs101_config.fragment | $(OUT)/linux
$(KERNEL_DOTCONFIG): $(KERNEL_STAMP)
	mkdir -p $(OUT)/linux/arch/arm64/configs
	KCONFIG_CONFIG=$(OUT)/linux/arch/arm64/configs/gs101_defconfig \
	    $(LINUX)/scripts/kconfig/merge_config.sh -m -r \
	        $(LINUX)/arch/arm64/configs/defconfig \
		src/pixelscripts/gs101_config.fragment
	+$(KERNEL_MAKE) gs101_defconfig

KERNEL_DOTCONFIG32 := $(OUT)/linux32/.config
$(KERNEL_DOTCONFIG32): $(KERNEL_STAMP)
	+$(KERNEL_MAKE) ARCH=arm O=$(KERNEL_OUT)/linux32 multi_v7_defconfig

.PHONY: configure
configure: $(KERNEL_DOTCONFIG)

.PHONY: copydefconfig menuconfig nconfig savedefconfig
menuconfig nconfig savedefconfig:
	+$(KERNEL_MAKE) $@

copydefconfig: savedefconfig
	@echo "Copying $(OUT)/linux/defconfig to $(LINUX)/arch/arm64/configs/defconfig"
	cp -a $(OUT)/linux/defconfig $(LINUX)/arch/arm64/configs/defconfig


BUILD_STAMP := $(OUT)/.kernel.built.stamp
$(BUILD_STAMP): $(KERNEL_STAMP) $(KERNEL_DOTCONFIG)
	+$(SCHED) $(IONICE) $(KERNEL_MAKE) -j$(PARALLEL) \
	    all
	touch $@

.PHONY: build
build: $(BUILD_STAMP)


ifeq ($(wildcard $(LINUX)/arch/arm64/boot/dts/google/gs101-oriole.dtso),)
# v3 and later
DIRS :=
BOARD_DTBO_BINARY := src/pixelscripts/$(BOARDDTB)o
else
# v2 and earlier
DIRS := $(OUT)/dtbo
BOARD_DTBO_BINARY := $(OUT)/dtbo/$(BOARDDTB)o
$(BOARD_DTBO_BINARY): $(BUILD_STAMP) | $(OUT)/dtbo
	cp --reflink=auto $(OUT)/linux/arch/arm64/boot/dts/google/$(BOARDDTB)o $@
endif

DIRS += $(OUT) $(OUT)/dtb $(OUT)/linux $(OUT)/images
$(DIRS):
	mkdir -p $@


$(OUT)/dtb/$(BOARDDTB): $(BUILD_STAMP) | $(OUT)/dtb
	find $(OUT)/linux/arch/arm64/boot/dts -path '*/google/$(BOARDDTB)' \
	    -exec cp --reflink=auto '{}' $(OUT)/dtb/ \;

$(OUT)/images/Image: $(BUILD_STAMP) | $(OUT)/images
	cp --reflink=auto $(OUT)/linux/arch/arm64/boot/Image $(OUT)/images/

ifneq ($(INTERNAL_PARALLEL_STRIP),)
# find should only run after modules have been installed because otherwise
# the list of files will be incorrect. We re-run make with
# INTERNAL_PARALLEL_STRIP set to ensure that. This way, we can run strip in
# parallel using as many jobs as make deems appropriate.
MODULES_TO_STRIP := $(shell find $(OUT)/images/lib/modules.nonstripped -name '*.ko')
STRIPPED_MODULES := $(subst .nonstripped/,/,$(MODULES_TO_STRIP))

$(STRIPPED_MODULES): $(OUT)/images/lib/modules/%.ko: $(OUT)/images/lib/modules.nonstripped/%.ko
	@$(STRIP) --strip-debug \
	         --remove-section=.comment --remove-section=.note \
		 --preserve-dates -o $@ $<

.PHONY: strip_installed_modules
strip_installed_modules: $(STRIPPED_MODULES)
endif

$(OUT)/.kernel.modules.installed.stamp: $(BUILD_STAMP)
	rm -rf $(OUT)/images/lib/modules*
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    INSTALL_MOD_PATH="../images" \
	    modules_install > /dev/null
	mv $(OUT)/images/lib/modules $(OUT)/images/lib/modules.nonstripped
	mkdir $(OUT)/images/lib/modules
	rsync --archive --hard-links --acls --whole-file --xattrs \
	      --sparse --preallocate --exclude '*.ko' \
	      $(OUT)/images/lib/modules.nonstripped/ \
	      $(OUT)/images/lib/modules
	$(MAKE) --no-print-directory -j$(PARALLEL) \
	    INTERNAL_PARALLEL_STRIP=1 strip_installed_modules
	touch $@

gz_cmd = $(KGZIP) --keep --stdout --fast $(1) > $(2)
xz_cmd = xz -T0 --compress --keep --stdout --fast $(1) > $(2)
lz4_cmd = lz4 --compress --keep --stdout --fast $(1) > $(2)

$(OUT)/images/Image.%: $(OUT)/images/Image | $(OUT)/images
	$(call $(*)_cmd,$<,$@)

$(OUT)/images/dtb.img: $(OUT)/dtb/$(BOARDDTB) | $(OUT)/images
	cp --reflink=auto $< $@

$(OUT)/images/dtbo.img: $(BOARD_DTBO_BINARY) | $(OUT)/images
	$(MKDTIMG) create $@ \
	    --page_size=4096 \
	    --id=/:board_id --rev=/:board_rev \
	    $<

$(OUT)/images/vendor-bootconfig.img: | $(OUT)/images
	echo "androidboot.boot_devices=14700000.ufs" > $(OUT)/images/vendor-bootconfig.img

INITRAMFS_CPIO_SOURCES_STAMP := $(OUT)/.initramfs.cpio.source.stamp
$(INITRAMFS_CPIO_SOURCES_STAMP): RELATIVE_OUT_DIR := $(shell realpath --relative-to=prebuilt-busybox-master $(OUT))
$(INITRAMFS_CPIO_SOURCES_STAMP): $(OUT)/FORCE | $(OUT)
	export GIT_INDEX_FILE=$(RELATIVE_OUT_DIR)/$(@F).gitindex \
	&& cd prebuilt-busybox-master \
	&& cp .git/index "$${GIT_INDEX_FILE}" \
	&& git --git-dir=./.git add -A . \
	&& git --git-dir=./.git write-tree > $(RELATIVE_OUT_DIR)/$(@F).tmp \
	&& rm "$${GIT_INDEX_FILE}"
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

PREBUILT_CPIO_BUSYBOX := $(OUT)/images/prebuilt.cpio

$(PREBUILT_CPIO_BUSYBOX): RELATIVE_OUT_DIR = $(shell realpath --relative-to=prebuilt-busybox-master/rootfs $(OUT)/images)
$(PREBUILT_CPIO_BUSYBOX): $(INITRAMFS_CPIO_SOURCES_STAMP) | $(OUT)/images
	cd prebuilt-busybox-master/rootfs \
	&& find . -print0 \
	   | sort -z \
	   | cpio --null -H newc -o \
	   > $(RELATIVE_OUT_DIR)/$(@F)

INITRAMFS_CPIO_PREBUILT_LOCATION_STAMP := $(OUT)/.initramfs.cpio.location.stamp
$(INITRAMFS_CPIO_PREBUILT_LOCATION_STAMP): $(OUT)/FORCE | $(OUT)
	echo $(PREBUILT_CPIO) > '$@.tmp'
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

$(OUT)/images/initramfs.cpio: $(OUT)/.kernel.modules.installed.stamp
$(OUT)/images/initramfs.cpio: $(INITRAMFS_CPIO_PREBUILT_LOCATION_STAMP)
$(OUT)/images/initramfs.cpio: $(PREBUILT_CPIO) | $(OUT)/images
	cp --reflink=auto '$<' '$@.tmp'
	cd $(OUT)/images \
	&& if ! find lib/modules -print0 \
	           | sort -z \
	           | cpio --null -H newc -o --append --file='$(@F).tmp' ; then \
	       rm '$(@F).tmp' ;\
	       exit 1 ;\
	    fi
	mv '$@.tmp' '$@'

$(OUT)/images/initramfs.cpio.%: $(OUT)/images/initramfs.cpio | $(OUT)/images
	$(call $(*)_cmd,$<,$@)

# we also add $(OUT)/FORCE in case it's passed in from the environment
$(OUT)/.kernelcmdline: $(OUT)/FORCE $(THIS_MAKEFILE)
	echo $(KERNELCMDLINE) > $@.tmp
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

FLASHABLE_IMAGES := $(OUT)/images/boot.img $(OUT)/images/vendor_boot.img $(OUT)/images/dtbo.img

$(OUT)/images/boot.img: $(OUT)/images/Image.lz4
$(OUT)/images/vendor_boot.img: $(OUT)/.kernelcmdline
$(OUT)/images/vendor_boot.img: $(OUT)/images/dtb.img
$(OUT)/images/vendor_boot.img: $(OUT)/images/initramfs.cpio.gz
$(OUT)/images/vendor_boot.img: $(OUT)/images/vendor-bootconfig.img
# &: requires GNU Make 4.3 or newer (Rules with Grouped Targets)
$(OUT)/images/boot.img $(OUT)/images/vendor_boot.img &: | $(OUT)/images
	$(MKBOOTIMG) --header_version 4 \
	    --dtb $(OUT)/images/dtb.img \
	    --kernel $(OUT)/images/Image.lz4 \
	    --vendor_bootconfig $(OUT)/images/vendor-bootconfig.img \
	    --vendor_cmdline "$(KERNELCMDLINE)" \
	    --vendor_ramdisk $(OUT)/images/initramfs.cpio.gz \
	    \
	    --vendor_boot $(OUT)/images/vendor_boot.img \
	    --output $(OUT)/images/boot.img

# these are the images that can (and need to) be flashed
.PHONY: images
images: $(FLASHABLE_IMAGES)


DT_BINDING_CHECK_VENDORS := exynos google samsung
DT_BINDING_CHECK_VENDOR_TARGETS := $(foreach v,$(DT_BINDING_CHECK_VENDORS),dt_binding_check_vendor_$(v))
DTBS_CHECK_ALL_TARGETS := dt_binding_check $(DT_BINDING_CHECK_VENDOR_TARGETS) dt_binding_check_vendors
DTBS_CHECK_ALL_TARGETS += dtbs_check_oriole dtbs_check
DTBS_CHECK_ALL_TARGETS += dtbs_check_exynos32 dtbs_check_exynos64

.PHONY: $(DTBS_CHECK_ALL_TARGETS)
$(DTBS_CHECK_ALL_TARGETS): $(KERNEL_STAMP)

$(OUT)/linux/scripts/dtc/dtc: $(KERNEL_STAMP) $(KERNEL_DOTCONFIG)
	+$(KERNEL_MAKE) -j$(PARALLEL) scripts_dtc

dtbs_check: $(OUT)/linux/scripts/dtc/dtc $(KERNEL_DOTCONFIG)
	+$(KERNEL_MAKE) W=1 dtbs_check

dtbs_check_exynos32: $(KERNEL_DOTCONFIG32)
	+$(KERNEL_MAKE) ARCH=arm O=$(KERNEL_OUT)/linux32 \
	    W=1 DT_CHECKER_FLAGS=-m CHECK_DTBS=y \
	    $$(set -x ; find $(LINUX)/arch/arm/boot/dts -name '*exynos*.dts' \
	       | sed -e 's|^$(LINUX)/arch/arm[^/]*/boot/dts/||' \
	             -e 's|\.dts$$|.dtb|')

dtbs_check_exynos64: $(OUT)/linux/scripts/dtc/dtc $(KERNEL_DOTCONFIG)
	+$(KERNEL_MAKE) \
	    W=1 DT_CHECKER_FLAGS=-m CHECK_DTBS=y \
	    $$(set -x ; find $(LINUX)/arch/arm64/boot/dts -name '*exynos*.dts' \
	       | sed -e 's|^$(LINUX)/arch/arm[^/]*/boot/dts/||' \
	             -e 's|\.dts$$|.dtb|')

dtbs_check_exynos: dtbs_check_exynos32 dtbs_check_exynos64

dtbs_check_oriole: $(OUT)/linux/scripts/dtc/dtc $(KERNEL_DOTCONFIG)
	@# remove existing DTBs, so kbuild reruns dt-validate
	rm -rf $(OUT)/linux/arch/arm64/boot/dts/exynos/google
	+$(KERNEL_MAKE) W=2 DT_CHECKER_FLAGS=-m CHECK_DTBS=y \
	    $$(find $(LINUX)/arch/arm64/boot/dts -name gs101-oriole.dts \
	       | sed -e 's|^$(LINUX)/arch/arm64/boot/dts/||' \
	             -e 's|\.dts$$|.dtb|')

dt_binding_check: $(OUT)/linux/scripts/dtc/dtc
	+$(KERNEL_MAKE) W=2 DT_CHECKER_FLAGS=-m $@

$(DT_BINDING_CHECK_VENDOR_TARGETS): $(OUT)/linux/scripts/dtc/dtc
	+$(KERNEL_MAKE) W=2 DT_CHECKER_FLAGS=-m \
	    DT_SCHEMA_FILES=$(subst dt_binding_check_vendor_,,$@) \
	    dt_binding_check

dt_binding_check_vendors: $(OUT)/linux/scripts/dtc/dtc
	+$(KERNEL_MAKE) W=2 DT_CHECKER_FLAGS=-m \
	    DT_SCHEMA_FILES=$(shell echo $(DT_BINDING_CHECK_VENDORS) | sed 's| \+|:|g') \
	    dt_binding_check

.PHONY: validate_dt
validate_dt: dt_binding_check_vendors dtbs_check_oriole

.PHONY: validate_dt2
validate_dt2: dt_binding_check dtbs_check



.PHONY: prepare-device
prepare-device:
	fastboot oem disable-verity
	fastboot oem disable-verification
	fastboot oem uart enable
	#fastboot oem uart disable
	fastboot oem uart config 3000000
	fastboot oem ramdump disable
	@# ensure no previous state in case flashing goes wrong in any way
	fastboot erase boot
	fastboot erase dtbo
	fastboot erase vendor_boot

.PHONY: flash flash-no-boot reflash reflash-no-boot
flash-no-boot: $(FLASHABLE_IMAGES)
flash-no-boot reflash-no-boot: prepare-device
flash-no-boot reflash-no-boot:
	@#fastboot flash dtbo $(OUT)/images/dtbo.img
	fastboot flash vendor_boot $(OUT)/images/vendor_boot.img
	fastboot flash boot $(OUT)/images/boot.img

flash reflash:
	$(MAKE) --no-print-directory $@-no-boot
	fastboot continue

.PHONY: run rerun
run: $(OUT)/images/boot.img $(OUT)/images/vendor_boot.img
run rerun:
	fastboot stage $(OUT)/images/vendor_boot.img
	fastboot boot $(OUT)/images/boot.img
